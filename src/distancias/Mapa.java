package distancias;

import java.util.HashMap;

public class Mapa {
	private HashMap<String, Nodo> nodos;
	
	public Mapa() {
		this.nodos = new HashMap<>();
		
		this.nodos.put("Madrid", new Nodo("Madrid", 40.42323, -3.69946, 4_000_000));
		this.nodos.put("Barcelona", new Nodo("Barcelona", 41.39760, 2.17314, 2_000_000));
		this.nodos.put("Ciudad Real", new Nodo("Ciudad Real", 38.98558, -3.92750, 70_000));
		this.nodos.put("Valdepeñas", new Nodo("Valdepeñas", 38.76365, -3.38918, 40_000));
	}
	
	public double getDistancia(String a, String b) {
		Nodo nodoA = this.nodos.get(a);
		Nodo nodoB = this.nodos.get(b);
		
		return Mapa.distance(nodoA.getLatitud(), nodoA.getLongitud(), nodoB.getLatitud(), nodoB.getLongitud());
	}
		
	public static double distance(double lat1, double long1, double lat2, double long2) {
		double R, dLat, dLong, a, c, d;
		R = 6378.137;
		dLat  = rad(lat2-lat1);
		dLong = rad(long2-long1);
		a = Math.sin(dLat)/2 * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2); 
		c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		d = R*c;
		return d;
 	}
	
	private static double rad(double alpha) {
		return alpha*Math.PI/180;
	}

	public int getPopulation(String nombre) {
		return this.nodos.get(nombre).getPopulation();
	}
}
