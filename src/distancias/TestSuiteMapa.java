package distancias;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import metamodelo.MetamorphicRelationship;

public class TestSuiteMapa {
	private Mapa mapa = new Mapa();
	
	@ParameterizedTest
	@MetamorphicRelationship(
		originalOracle = "youtube.getResults(termino, filtro).size()>0",
		expectedResult = "youtube.getResults(termino, filtro).size()==youtube.getResults(termino,'rating').size()"
	)
	@CsvSource(value = {"Winter:Relevance", "Invierno:Relevance" }, delimiter = ':')
	public void testYoutubeBusqueda(String termino, String filtro) {
	}

/*	@ParameterizedTest
	@MetamorphicRelationship(expectedResult = "mapa.getDistancia(origen, destino)>=mapa.getDistancia(destino, origen)*0.9 && mapa.getDistancia(origen, destino)<=mapa.getDistancia(destino, origen)*1.1")
	@CsvSource(value = {"Madrid:Barcelona", "Madrid:Ciudad Real" }, delimiter = ':')
	public void testDistancia(String origen, String destino) {
		
	}

	@ParameterizedTest
	@MetamorphicRelationship(expectedResult = "mapa.getPopulation(origen) + mapa.getPopulation(destino)=mapa.getPopulation(destino) + mapa.getPopulation(origen)")
	@CsvSource(value = {"Madrid:Barcelona:6_000_000", "Valdepeñas:Ciudad Real:110_000" }, delimiter = ':')
	public void testPoblacion(String origen, String destino) {
		
	}*/
}

/*
 
public void testYoutubeBusqueda1(String termino, String filtro) {
	assertTrue(youtube.getResults(termino, filtro).size()>0);
	assertTrue(youtube.getResults(termino, filtro).size()==youtube.getResults(termino,'rating').size());
}

public void testYoutubeBusqueda2(String termino, String filtro) {
	assertTrue(youtube.getResults(termino, filtro).size()>0);
	assertTrue(youtube.getResults(termino, filtro).size()==youtube.getResults(termino,'rating').size());
}

 
 
 
 
 
testPoblacion1("Madrid", "Barcelona")
testPoblacion1_MR("Barcelona", "Madrid")

testPoblacion2("Valdepeñas", "Ciudad Real")
testPoblacion2_MR("Ciudad Real", "Valdepeñas")
*/