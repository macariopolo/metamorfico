package distancias;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import metamodelo.MTestSuite;
import metamodelo.MetamorphicRelationship;

public class AnalizadorDeTests {
	
	public static void main(String[] args) throws Exception {
		Class<?> testClass = Class.forName("distancias.TestSuiteMapa");
		Method[] methods = testClass.getDeclaredMethods();
		ArrayList<Method> testMethods = new ArrayList<>();
		for (Method method : methods) {
			if (Modifier.isPublic(method.getModifiers())) {
				if (method.isAnnotationPresent(ParameterizedTest.class)) {
					testMethods.add(method);
				}
			}
		}
		
		for (Method junitTestMethod : testMethods) {
			MTestSuite mts = new MTestSuite();
			mts.setName(junitTestMethod.getName());
			
			CsvSource cvsAnnotation = junitTestMethod.getAnnotation(CsvSource.class);
			MetamorphicRelationship mrAnnotation = junitTestMethod.getAnnotation(MetamorphicRelationship.class);
			mts.setOriginalOracle(mrAnnotation.originalOracle());
			mts.setExpectedResult(mrAnnotation.expectedResult());
			String[] values = cvsAnnotation.value();
			char delimiter = cvsAnnotation.delimiter();
			for (String tupla : values) {
				String[] tokens = tupla.split("" + delimiter);
				mts.addTestCase(tokens);
			}

			System.out.println(mts.getCode());

			
		}
		
	}
}
