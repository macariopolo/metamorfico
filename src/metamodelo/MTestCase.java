package metamodelo;

import java.util.ArrayList;

public class MTestCase {
	private ArrayList<Object> values;
	
	public MTestCase() {
		this.values = new ArrayList<>();
	}

	public void addValue(String token) {
		this.values.add(token);
	}

	@Override
	public String toString() {
		String r = "";
		for (Object o : values)
			r+=o + "\t";
		return r;
	}

	public String getCode(MTestSuite testSuite) {
		String r = "public void " + testSuite.getName() + "() {\n";
		if (testSuite.getOriginalOracle().length()>0) {
			r = r + "\tassertTrue(";
			r = r + testSuite.getOriginalOracle();
			r = r  + ");\n";
			r = r + "}\n\n";
		}
		for (int i=0; i<this.values.size(); i++) {
			r = r + "\tassertTrue("  + testSuite.getExpectedResult() + ");\n";
		}
		return r;
	}
}
