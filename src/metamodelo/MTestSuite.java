package metamodelo;

import java.util.ArrayList;

public class MTestSuite {

	private String name;
	private ArrayList<MTestCase> testCases;
	private String originalOracle;
	private String expectedResult;

	public MTestSuite() {
		this.testCases = new ArrayList<>();
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void addTestCase(String[] tokens) {
		MTestCase mtc = new MTestCase();
		for (String token : tokens)
			mtc.addValue(token);
		this.testCases.add(mtc);
	}
	
	@Override
	public String toString() {
		String r = this.name;
		for (MTestCase mtc : this.testCases)
			r=r+mtc.toString();
		return r;
	}

	public String getCode() {
		String r = "";
		for (MTestCase mtc : this.testCases)
			r=r+mtc.getCode(this);
		return r;

	}

	public void setOriginalOracle(String originalOracle) {
		this.originalOracle = originalOracle;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public String getName() {
		return name;
	}

	public String getOriginalOracle() {
		return this.originalOracle;
	}

	public String getExpectedResult() {
		return this.expectedResult;
	}
}

/*

public void testYoutubeBusqueda1(String termino, String filtro) {
	assertTrue(youtube.getResults(termino, filtro).size()>0);
	assertTrue(youtube.getResults(termino, filtro).size()==youtube.getResults(termino,'rating').size());
}


*/